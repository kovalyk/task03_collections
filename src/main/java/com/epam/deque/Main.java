package com.epam.deque;

public class Main {
    public static void main(String[] args) {
        Deque deque = new Deque();
        System.out.println(deque);
        deque.addFirst(7);
        deque.addFirst(8);
        deque.addFirst(14);
        deque.addLast(15);
        deque.addFirst(18);
        System.out.println("DEQUE: " + deque);
        deque.removeFirst();
        deque.removeLast();
        System.out.println("DEQUE: " + deque);
    }
}
