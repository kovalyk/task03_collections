package com.epam.deque;

import java.util.NoSuchElementException;

public class Deque<E> {
    private Element last;
    private Element first;

    public Deque() {
        first = last = null;
    }

    public void addFirst(int data) {
        Element newElement = new Element(data);
        newElement.next = null;
        if (isEmpty()) {
            first = last = newElement;
        } else {
            first.prev = newElement;
            newElement.next = first;
            first = newElement;
        }
    }

    public int removeFirst() {
        int data = first.data;
        if (first.next == null)
            last = first = null;
        else {
            first.next.prev = null;
            first = first.next;
        }
        return data;
    }

    public void addLast(int data) {
        Element newElement = new Element(data);

        if (isEmpty()) {
            newElement.next = null;
            first = last = newElement;
        } else {
            last.next = newElement;
            newElement.prev = last;
            last = newElement;
        }
    }

    public int removeLast() {
        int data = last.data;
        if (first.next == null)
            first = last = null;
        else {
            last.prev.next = null;
            last = last.prev;
        }
        return data;
    }

    public boolean isEmpty() {
        return first == null;
    }

    public int getFirst() {
        if (first == null) {
            throw new NoSuchElementException();
        }
        return first.data;
    }

    public int getLast() {
        if (last == null) {
            throw new NoSuchElementException();
        }
        return last.data;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        Element element = first;
        while (element != null) {
            builder.append(element.data + " ");
            element = element.next;
        }
        return new String(builder);
    }

    private class Element {
        public Element next;
        public Element prev;
        public int data;

        public Element(int data) {
            this.data = data;
        }
    }
}

