package com.epam.priorityqueue;


import java.util.Arrays;

public class PriorityQueue<T extends Comparable<T>> {
    private int capacity = 10;
    private int size = 0;
    private T[] heap;

    public PriorityQueue() {
        heap = (T[]) new Comparable[capacity];
    }

    private int getLeftChildIndex(int parentIndex) {
        return 2 * parentIndex + 1;
    }

    private int getRightChildIndex(int parentIndex) {
        return 2 * parentIndex + 2;
    }

    private int getParentIndex(int childIndex) {
        return (childIndex - 1) / 2;
    }

    private boolean hasLeftChild(int index) {
        return getLeftChildIndex(index) < size;
    }

    private boolean hasRightChild(int index) {
        return getRightChildIndex(index) < size;
    }

    private boolean hasParent(int index) {
        return getParentIndex(index) >= 0;
    }

    private T leftChild(int index) {
        return heap[getLeftChildIndex(index)];
    }

    private T rightChild(int index) {
        return heap[getRightChildIndex(index)];
    }

    private T parent(int index) {
        return heap[getParentIndex(index)];
    }

    public void add(T item) {
        ensureCapacity();
        heap[size++] = item;
        moveUp();
    }

    private void swap(int indexOne, int indexTwo) {
        T temp = heap[indexOne];
        heap[indexOne] = heap[indexTwo];
        heap[indexTwo] = temp;
    }

    private void ensureCapacity() {
        if (size == capacity) {
            capacity += capacity >> 1;
            heap = Arrays.copyOf(heap, capacity);
        }
    }

    public T peek() {
        if (isEmpty()) {
            return null;
        } else {
            return heap[0];
        }
    }

    public T poll() {
        if (isEmpty()) {
            return null;
        } else {
            T item = heap[0];
            heap[0] = heap[--size];
            moveDown();
            return item;
        }
    }

    private void moveUp() {
        int index = size - 1;
        while (hasParent(index) && parent(index).compareTo(heap[index]) > 0) {
            swap(getParentIndex(index), index);
            index = getParentIndex(index);
        }
    }

    private void moveDown() {
        int index = 0;
        while (hasLeftChild(index)) {
            int smallerChildIndex = getLeftChildIndex(index);
            if (hasRightChild(index) && rightChild(index).compareTo(leftChild(index)) < 0) {
                smallerChildIndex = getRightChildIndex(index);
            }

            if (heap[index].compareTo(heap[smallerChildIndex]) < 0) {
                break;
            } else {
                swap(index, smallerChildIndex);
            }
            index = smallerChildIndex;
        }
    }

    public boolean isEmpty() {
        return size == 0;
    }
}
